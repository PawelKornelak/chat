package com.kornelak.notification;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.kornelak.AbstractRemoteService;
import com.kornelak.chat.message.Message;
import com.kornelak.chat.message.Message2;
import com.kornelak.chat.message.MessageDao;
import com.kornelak.chat.message.MessageService;

@RestController
@RequestMapping("/api/event")
public class GetNotificationFromRemoteServetController extends AbstractRemoteService {

	@Autowired
	private MessageService messageService;

	@Autowired
	private MessageDao messageDao;

	@Value("${remote.baseUrl}")
	private String remoteBaseUrl;

	private static final String USER_RESOURCE_URLM = "/api/event";

	@RequestMapping(method = RequestMethod.GET)
	public void getNotificationFromRemoteServer() {

		HttpHeaders headers = getDefaultHeaders();
		URI url = prepareUrl(USER_RESOURCE_URLM);

		RequestEntity<Void> request = new RequestEntity<>(headers, HttpMethod.GET, url);
		ResponseEntity<List<NotificationFromServer>> response = restTemplate.exchange(request,
				new ParameterizedTypeReference<List<NotificationFromServer>>() {
				});
//		System.out.println("From Remote GETALLEVENT " + response.getBody());

		List<NotificationFromServer> listeventsRemoteServer = new ArrayList();
		listeventsRemoteServer.addAll(response.getBody());
//		System.out.println("GET ALL EVENT amounts of event: "+listeventsRemoteServer.size());
		for (NotificationFromServer notificationFromServer : listeventsRemoteServer) {

			if (messageDao.checkIfMessageIdExist(notificationFromServer.getEventBody().getMessageId())) {

				if (notificationFromServer.getEventType().equals("RECEIVED_NOTIFICATION")) {
					System.out.println("Save Received Notification to messageId: "
							+ notificationFromServer.getEventBody().getMessageId());
					messageService
							.saveConfirmReceivedNotification(notificationFromServer.getEventBody().getMessageId());
				} else if (notificationFromServer.getEventType().equals("READ_NOTIFICATION")) {
					System.out.println("Save Read Notification to messageId: "
							+ notificationFromServer.getEventBody().getMessageId());
					messageService.saveConfirmReadNotification(notificationFromServer.getEventBody().getMessageId());
				}

			}
		}
//		System.out.println("koniec event");
	}

}
